# <a href="https://t.me/viktormazepa_bot">Telegram kBot</a>
## Link - <a href="https://t.me/viktormazepa_bot">https://t.me/viktormazepa_bot</a>

#### Project is used for GitLub CI investigation
The code is stored in ```https://github.com/viktor-mazepa/kbot```

### GitLub CI steps
* User has possibility to change target OS and architecture before start:
![img.png](doc/img/img.png)
* Clone git repository
* Run tests
* Build application
* Create docker image
* Push docker image to GitLub Container Registry
![img.png](doc/img/img1.png)
* Current pipeline use custom Ranner, that connected to 
the local machine with Ubuntu OS
![img.png](doc/img/img2.png)

## GitLab Pipelines vs Jenkins - pros/cons 

### GitLab Pipelines:

#### Pros:

* **Integration with GitLab**: Seamless integration with GitLab repositories, making it easy to set up and manage CI/CD within the same platform.
* **YAML Configuration**: Pipelines are configured using YAML files, which can be version-controlled along with the code, ensuring versioning consistency.
* **Easy to Use**: User-friendly interface for setting up CI/CD pipelines, reducing the learning curve for new users.
* **Built-in Security**: Security scanning and testing tools are integrated into the pipeline, allowing for automated security checks.
* **Scalability**: Well-suited for small to medium-sized projects and teams due to its ease of setup and use.
* **Service**: No additional physical environment or Kubernetes cluster is required for installation.


#### Cons:

* **Limited Flexibility**: While powerful for simpler projects, it might lack some of the advanced features and flexibility that Jenkins offers for complex CI/CD workflows.
* **Community Support**: Might have a smaller pool of plugins and community support compared to Jenkins.
* **Dependency on GitLab**: Since it's tightly integrated with GitLab, if there are issues with GitLab, it might affect pipeline functionality.
* **Less Extensive Ecosystem**: Might not have the extensive plugin ecosystem and integrations that Jenkins offers.

### Jenkins:

#### Pros:

* **Extensive Plugin Ecosystem**: Vast array of plugins available for customization, enabling support for various tools, languages, and integrations.
* **Flexibility**: Highly configurable and customizable, allowing for complex CI/CD workflows and automation.
* **Community Support**: Large and active community providing support, documentation, and contributions.
* **Independence**: Not tied to any specific version control system or platform, allowing it to integrate with various tools and services.
* **Scalability**: Well-suited for large projects and enterprises due to its flexibility and customization options.

#### Cons:

* **Steep Learning Curve**: Can be challenging for new users due to its complexity and myriad of options.
* **Maintenance Overhead**: Requires regular updates, maintenance, and plugin management, which can be time-consuming.
* **UI/UX**: User interface might not be as intuitive as some of the newer CI/CD tools like GitLab.
* **Resource Intensive**: Jenkins might require more resources (CPU, memory) compared to some other CI/CD tools.